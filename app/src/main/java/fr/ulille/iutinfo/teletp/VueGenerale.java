package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle,poste;
    private String DISTANCIEL;

    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        poste = "";
        DISTANCIEL = getActivity().getResources().getStringArray(R.array.list_salles)[0];
        salle = DISTANCIEL;

        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapterSalle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spSalle.setAdapter(adapterSalle);
        spPoste.setAdapter(adapterPoste);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView twLogin = (TextView)getActivity().findViewById(R.id.tvLogin);
            model.setUsername(twLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Courage pour les corrections, prennez un café !
            }
        });
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Ou prennez un thé, ça peut faire du bien de changer de temps en temps
            }
        });
        update();

        // TODO Q9
    }

    // TODO Q5.a
    private void update(){
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        if(getActivity().getResources().getStringArray(R.array.list_salles)[spSalle.getSelectedItemPosition()].equals(DISTANCIEL)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation(DISTANCIEL);
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(getActivity().getResources().getStringArray(R.array.list_salles)[spSalle.getSelectedItemPosition()]+" : "
                   +getActivity().getResources().getStringArray(R.array.list_postes)[spPoste.getSelectedItemPosition()]);
        }
    }

    // TODO Q9
}